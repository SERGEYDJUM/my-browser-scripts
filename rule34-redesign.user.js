// ==UserScript==
// @name        Rule34 Redesign
// @namespace   Violentmonkey Scripts
// @match       https://rule34.xxx/index.php?page=post&s=list*
// @run-at      document-idle
// @require     https://releases.jquery.com/git/jquery-git.min.js
// @grant       GM_addStyle
// @grant       GM_download
// @version     1.2.0
// @author      crimson_one
// @description Improves UX of rule34.xxx
// ==/UserScript==


// Configs
const delay = 400;


// Styles
GM_addStyle(`
    * { --c-bg: #0f0f0f; --c-bg-alt: #101010; }
    #paginator {position: fixed; bottom: 0; right: 10px; z-index: 2}
    body {max-width: 100vw; }
    div.image-list { display: flex; flex-wrap: wrap; }
    #preview-item { width: 720px; object-fit: contain; }
    #preview-item, .thumb > a > img { border-radius: 8px; }
`);


// Globals
var timer;


// Logic
$("#post-list").append(`<div id="preview-bar"></div>`);
$("div.sidebar").toggle();

$("body").on("dblclick", () => {
    $("div.sidebar").toggle();
});

function update_preview_with(url) {
    $.get(url, (data) => {
        var src_url = new URL($(data).find(`a:contains("Original image")`).attr("href"));
        src_url.search = '';

        if (src_url.pathname.includes("mp4")) {
            $("#preview-bar").html(`<video controls id="preview-item" src="${src_url.href}"></video>`);
        } else {
            $("#preview-bar").html(`<img id="preview-item" src="${src_url.href}">`);
        }

        $("#preview-item").on("contextmenu", (e) => {
            e.preventDefault();
            const url = e.target.src;
            GM_download(url, url.split("/").pop());
        });
    });
}

$("img.preview").on("mouseover", (e) => {
    timer = setTimeout(() => update_preview_with(e.target.parentElement.href), delay);
});

$("img.preview").on("mouseout", () => clearTimeout(timer));