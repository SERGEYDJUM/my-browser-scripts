// ==UserScript==
// @name        Newgrounds Batch Downloader 1/2
// @namespace   Violentmonkey Scripts
// @match       https://*.newgrounds.com/art
// @run-at      document-idle
// @grant       GM_registerMenuCommand
// @grant       GM_openInTab
// @require     https://releases.jquery.com/git/jquery-git.slim.min.js
// @version     2.0.1
// @author      crimson_one
// @description Requires Newgrounds Batch Downloader 2/2
// ==/UserScript==

function process_url(url) {
    console.info(`Opening page: ${url} ...`);
    const url_obj = new URL(url);
    url_obj.search = '?trigger=1'
    GM_openInTab(url_obj.toString());
}

GM_registerMenuCommand("Batch Download", () => {
    $('.item-portalitem-art-medium').each((i, el) => {
        const href = $(el).attr("href");
        setTimeout(() => process_url(href), 5000*i);
    })
})
