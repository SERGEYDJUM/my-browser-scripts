// ==UserScript==
// @name        Newgrounds Batch Downloader 2/2
// @namespace   Violentmonkey Scripts
// @match       https://www.newgrounds.com/art/view/*/*
// @run-at      document-idle
// @grant       GM_download
// @require     https://releases.jquery.com/git/jquery-git.slim.min.js
// @version     2.0.3
// @author      crimson_one
// @description Requires Newgrounds Batch Downloader 1/2
// ==/UserScript==

function download_item(url) {
    console.info(`Downloading image ${url} ...`);

    const url_obj = new URL(url);
    url_obj.search = '';

    GM_download(url_obj.toString(), url_obj.toString().split('/').pop())
}

function process_page() {
    console.info("Searching for images...");

    $("div.image > a, div.art-item-container > a").each((i, el) => {
        const href = $(el).attr("href");
        setTimeout(() => download_item(href), 1000*i);
    });
}

const url_obj = new URL(window.location.href)
if (url_obj.searchParams.get("trigger") == "1") {
    process_page();
    setTimeout(() => window.close(), 10000);
}
