// ==UserScript==
// @name        Gelbooru Redesign
// @namespace   Violentmonkey Scripts
// @match       https://gelbooru.com/index.php?page=post&s=list*
// @run-at      document-idle
// @require     https://releases.jquery.com/git/jquery-git.min.js
// @grant       GM_addStyle
// @grant       GM_download
// @version     0.2.0
// @author      crimson_one
// @description Improves UX of gelbooru.com
// ==/UserScript==


// Configs
const delay = 600;
const video_types = ["mp4", "webm", "mkv", "avi"];
const video_checker = s => video_types.some(ext => s.includes(ext));

// Styles
GM_addStyle(`
    body {max-width: 100vw;}
    main {display: flex; align-items: center;}
    #container {display: block; overflow: hidden;}
    #paginator {position: fixed; bottom: 0; right: 0; z-index: 2; display: flex; flex-direction: column;}
    #preview-bar {min-width: 30vw; max-width: 50vw; max-height: 94vh; padding: 8px;}
    #preview-item {border-radius: 16px; width: 100%;}
    .thumbnail-container, #preview-bar {overflow: scroll; scrollbar-width: none;}
    .thumbnail-container {height: 94vh;}
    .thumbnail-preview {height: unset; width: unset; margin: unset; padding: 8px;}
    .thumbnail-preview > a > img {border-radius: 6px; max-width: 250px; max-height: 250px;}
    .sm-hidden, .aside {display: none;}
`);

// Globals
var timer;
var original_src;

// Logic
function update_preview_with(url) {
    $.get(url, (data) => {
        var src_url = null
        original_src = new URL($(data).find(`a:contains("Original image")`).attr("href"))
        original_src.search = '';

        if ($(data).find(`#image`).length) {
            src_url = new URL($(data).find(`#image`).attr("src"));
            src_url.search = '';
        } else {
            src_url = original_src;
        }

        $("#preview-item").off();

        if (video_checker(src_url.pathname)) {
            $("#preview-bar").html(`<video controls id="preview-item" src="${src_url.href}"></video>`);
        } else {
            $("#preview-bar").html(`<img id="preview-item" src="${src_url.href}">`);
        }

        $("#preview-item").on("contextmenu", (e) => {
            e.preventDefault();
            var url = original_src.pathname;
            GM_download(url, url.split("/").pop());
        });
    });
}

$("main").prepend(`<div id="preview-bar"></div>`);

$("body").on("dblclick", () => {
    $(".aside").toggle();
});

$(".thumbnail-preview > a").on("mouseover", (e) => {
    timer = setTimeout(() => update_preview_with(e.currentTarget.href), delay);
});

$(".thumbnail-preview > a").on("mouseout", () => clearTimeout(timer));